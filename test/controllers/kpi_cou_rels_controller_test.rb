require 'test_helper'

class KpiCouRelsControllerTest < ActionController::TestCase
  setup do
    @kpi_cou_rel = kpi_cou_rels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:kpi_cou_rels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create kpi_cou_rel" do
    assert_difference('KpiCouRel.count') do
      post :create, kpi_cou_rel: { co_id: @kpi_cou_rel.co_id, kp_id: @kpi_cou_rel.kp_id }
    end

    assert_redirected_to kpi_cou_rel_path(assigns(:kpi_cou_rel))
  end

  test "should show kpi_cou_rel" do
    get :show, id: @kpi_cou_rel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @kpi_cou_rel
    assert_response :success
  end

  test "should update kpi_cou_rel" do
    patch :update, id: @kpi_cou_rel, kpi_cou_rel: { co_id: @kpi_cou_rel.co_id, kp_id: @kpi_cou_rel.kp_id }
    assert_redirected_to kpi_cou_rel_path(assigns(:kpi_cou_rel))
  end

  test "should destroy kpi_cou_rel" do
    assert_difference('KpiCouRel.count', -1) do
      delete :destroy, id: @kpi_cou_rel
    end

    assert_redirected_to kpi_cou_rels_path
  end
end
