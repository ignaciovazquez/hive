# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Hive::Application.config.secret_key_base = 'c3dc0380f897275fd4e78f240e222196ff72588f16f7c39062e35c104eba56541ed8a5208ce77db9366a8d58917080e8dfdbb830491b96230ed4be2437886165'
