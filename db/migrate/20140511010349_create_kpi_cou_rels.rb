class CreateKpiCouRels < ActiveRecord::Migration
  def change
    create_table :kpi_cou_rels do |t|
     	t.belongs_to :counter
	t.belongs_to :kpi
      	t.timestamps
    end
  end
end
