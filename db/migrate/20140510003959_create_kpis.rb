class CreateKpis < ActiveRecord::Migration
  def change
    create_table :kpis do |t|
	t.belongs_to :type
      t.string :kp_name
      t.string :kp_formula
      t.string :kp_unit

      t.timestamps
    end
  end
end
