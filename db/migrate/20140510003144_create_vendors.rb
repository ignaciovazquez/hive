class CreateVendors < ActiveRecord::Migration
  def change
    create_table :vendors do |t|
      t.string :ve_name

      t.timestamps
    end
  end
end
