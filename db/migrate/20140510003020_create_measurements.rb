class CreateMeasurements < ActiveRecord::Migration
  def change
    create_table :measurements do |t|
     	t.belongs_to :vendor
	t.belongs_to :adapter
     	t.string :me_name
     	t.string :me_table
      	t.timestamps
    end
  end
end
