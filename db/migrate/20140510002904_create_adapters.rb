class CreateAdapters < ActiveRecord::Migration
  def change
    create_table :adapters do |t|
      t.string :ad_name

      t.timestamps
    end
  end
end
