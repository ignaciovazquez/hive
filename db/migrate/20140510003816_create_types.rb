class CreateTypes < ActiveRecord::Migration
  def change
    create_table :types do |t|
      t.string :ty_name
      t.timestamps
    end
  end
end
