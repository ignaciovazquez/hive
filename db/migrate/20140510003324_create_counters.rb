class CreateCounters < ActiveRecord::Migration
  def change
    create_table :counters do |t|
	t.belongs_to :measurement
      	t.string :co_name
      	t.string :co_column
      	t.string :co_desc
      	t.string :co_unit
      	t.timestamps
    end
  end
end
