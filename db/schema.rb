# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140511010349) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "adapters", force: true do |t|
    t.string   "ad_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "counters", force: true do |t|
    t.integer  "measurement_id"
    t.string   "co_name"
    t.string   "co_column"
    t.string   "co_desc"
    t.string   "co_unit"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "kpi_cou_rels", force: true do |t|
    t.integer  "counter_id"
    t.integer  "kpi_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "kpis", force: true do |t|
    t.integer  "type_id"
    t.string   "kp_name"
    t.string   "kp_formula"
    t.string   "kp_unit"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "measurements", force: true do |t|
    t.integer  "vendor_id"
    t.integer  "adapter_id"
    t.string   "me_name"
    t.string   "me_table"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "types", force: true do |t|
    t.string   "ty_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vendors", force: true do |t|
    t.string   "ve_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
