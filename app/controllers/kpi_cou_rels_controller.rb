class KpiCouRelsController < ApplicationController
  before_action :set_kpi_cou_rel, only: [:show, :edit, :update, :destroy]

  # GET /kpi_cou_rels
  # GET /kpi_cou_rels.json
  def index
    @kpi_cou_rels = KpiCouRel.all
  end

  # GET /kpi_cou_rels/1
  # GET /kpi_cou_rels/1.json
  def show
  end

  # GET /kpi_cou_rels/new
  def new
    @kpi_cou_rel = KpiCouRel.new
  end

  # GET /kpi_cou_rels/1/edit
  def edit
  end

  # POST /kpi_cou_rels
  # POST /kpi_cou_rels.json
  def create
    @kpi_cou_rel = KpiCouRel.new(kpi_cou_rel_params)

    respond_to do |format|
      if @kpi_cou_rel.save
        format.html { redirect_to @kpi_cou_rel, notice: 'Kpi cou rel was successfully created.' }
        format.json { render action: 'show', status: :created, location: @kpi_cou_rel }
      else
        format.html { render action: 'new' }
        format.json { render json: @kpi_cou_rel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kpi_cou_rels/1
  # PATCH/PUT /kpi_cou_rels/1.json
  def update
    respond_to do |format|
      if @kpi_cou_rel.update(kpi_cou_rel_params)
        format.html { redirect_to @kpi_cou_rel, notice: 'Kpi cou rel was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @kpi_cou_rel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kpi_cou_rels/1
  # DELETE /kpi_cou_rels/1.json
  def destroy
    @kpi_cou_rel.destroy
    respond_to do |format|
      format.html { redirect_to kpi_cou_rels_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_kpi_cou_rel
      @kpi_cou_rel = KpiCouRel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def kpi_cou_rel_params
      params.require(:kpi_cou_rel).permit(:kp_id, :co_id)
    end
end
