class KpisController < ApplicationController
  before_action :set_kpi, only: [:show, :edit, :update, :destroy]

  # GET /kpis
  # GET /kpis.json
  def index
    @kpis = Kpi.all
  end

  # GET /kpis/1
  # GET /kpis/1.json
  def show
  end

  # GET /kpis/new
  def new
    @kpi = Kpi.new
  end

  # GET /kpis/1/edit
  def edit
  end

  # POST /kpis
  # POST /kpis.json
  def create
    @type = Type.find(kpi_params[:type_id])
    @kpi = @type.kpis.create(kpi_params)

    respond_to do |format|
      if @kpi.save
        format.html { redirect_to @kpi, notice: 'Kpi was successfully created.' }
        format.json { render action: 'show', status: :created, location: @kpi }
      else
        format.html { render action: 'new' }
        format.json { render json: @kpi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kpis/1
  # PATCH/PUT /kpis/1.json
  def update
    respond_to do |format|
      if @kpi.update(kpi_params)
        format.html { redirect_to @kpi, notice: 'Kpi was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @kpi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kpis/1
  # DELETE /kpis/1.json
  def destroy
    @kpi.destroy
    respond_to do |format|
      format.html { redirect_to kpis_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_kpi
      @kpi = Kpi.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def kpi_params
      params.require(:kpi).permit(:type_id, :kp_name, :kp_formula, :kp_unit)
    end
end
