class Adapter < ActiveRecord::Base
	has_many :measurements, dependent: :destroy
	has_many :counters, :through => :measurement
	validates_presence_of :ad_name
end
