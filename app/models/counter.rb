class Counter < ActiveRecord::Base
	belongs_to :measurement
	has_many :kpiCouRels, dependent: :destroy
	has_many :kpis, through: :kpiCouRels
	validates_presence_of :measurement_id
	validates_presence_of :co_name
	validates_presence_of :co_column
	validates_presence_of :co_desc
	validates_presence_of :co_unit
end
