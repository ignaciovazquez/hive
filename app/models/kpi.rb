class Kpi < ActiveRecord::Base
	belongs_to :type
	has_many :kpiCouRels, dependent: :destroy
	has_many :counters, through: :kpiCouRels
	validates_presence_of :type_id
	validates_presence_of :kp_name
	validates_presence_of :kp_formula
	validates_presence_of :kp_unit
end
