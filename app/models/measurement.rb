class Measurement < ActiveRecord::Base
	belongs_to :adapter
	belongs_to :vendor
	has_many :counters, dependent: :destroy
	validates_presence_of :me_name
	validates_presence_of :me_table
end
