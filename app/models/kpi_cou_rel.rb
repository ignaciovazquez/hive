class KpiCouRel < ActiveRecord::Base
	belongs_to :counter
	belongs_to :kpi
	validates_presence_of :kp_id
	validates_presence_of :co_id
end
