class Type < ActiveRecord::Base
	has_many :kpis, dependent: :destroy
	validates_presence_of :ty_name
end
