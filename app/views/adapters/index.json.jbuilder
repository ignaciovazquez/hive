json.array!(@adapters) do |adapter|
  json.extract! adapter, :id, :ad_id, :ad_name
  json.url adapter_url(adapter, format: :json)
end
