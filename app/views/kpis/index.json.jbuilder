json.array!(@kpis) do |kpi|
  json.extract! kpi, :id, :kp_id, :ty_id, :kp_name, :kp_formula, :kp_unit
  json.url kpi_url(kpi, format: :json)
end
