json.array!(@vendors) do |vendor|
  json.extract! vendor, :id, :ve_id, :ve_name
  json.url vendor_url(vendor, format: :json)
end
