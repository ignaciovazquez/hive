json.array!(@measurements) do |measurement|
  json.extract! measurement, :id, :me_id, :ve_id, :ad_id, :me_name, :me_table
  json.url measurement_url(measurement, format: :json)
end
