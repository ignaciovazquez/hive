json.array!(@kpi_cou_rels) do |kpi_cou_rel|
  json.extract! kpi_cou_rel, :id, :kp_id, :co_id
  json.url kpi_cou_rel_url(kpi_cou_rel, format: :json)
end
