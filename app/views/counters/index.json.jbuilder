json.array!(@counters) do |counter|
  json.extract! counter, :id, :co_id, :me_id, :co_name, :co_column, :co_desc, :co_unit
  json.url counter_url(counter, format: :json)
end
