json.array!(@types) do |type|
  json.extract! type, :id, :ty_id, :ty_name
  json.url type_url(type, format: :json)
end
